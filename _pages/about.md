---
layout: page
title: About the OpenOximeter project
permalink: /about/
---

The OpenOximeter project is a interdisciplinary project at the University of Bath. We would be very happy to form collaborations with other groups that want to work on the device.

## Department of Electrical Engineering
* [Peter Wilson](https://researchportal.bath.ac.uk/en/persons/peter-wilson)
* [Jonathan Graham-Harper-Cater](https://researchportal.bath.ac.uk/en/persons/jonathan-graham-harper-cater)
* [Benjamin Metcalfe](https://researchportal.bath.ac.uk/en/persons/benjamin-metcalfe)

## Department of Mechanical Engineering
* [Pejman Iravani](https://researchportal.bath.ac.uk/en/persons/pejman-iravani)

## Department of Physics
* [Richard Bowman](https://researchportal.bath.ac.uk/en/persons/richard-bowman)
* [Julian Stirling](https://researchportal.bath.ac.uk/en/persons/julian-stirling)
