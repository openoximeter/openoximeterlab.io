---
layout: home
---

The OpenOximeter project was started at the University of Bath. It was deigned to be an advisory device for hospitals in the COVID crisis. As an advisory device it would only be used on people who are believed healthier to identify if they start to crash and a medically proven device must then be fitted. So far the hospital we have been in contact with has not needed the device. As such, it is untested even for this use. 

The OpenOximeter is currently in an early prototype stage. It is never safe to build your own medical equipment. This project is open to encourage experts to work together to build the best and most adaptable solution.

In the long term we would like the project to develop into a medically certified device, a lot of work is needed before this is possible. However, it is important to note that the design itself will never be "medically certified". Individual manufacturers with the appropriate manufacturing capabilities and quality management systems will always be needed to certify a device. This is essential to ensuring safety.

The current [OpenOximeter design is on GitLab](https://gitlab.com/openoximeter). Please feel free to get involved in the design.
